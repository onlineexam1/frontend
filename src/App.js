import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import React from 'react';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import axios from 'axios';
import ExamList from './components/ExamList.js';
import ExamDetail from './components/ExamDetail.js';
import ExamTake from './components/ExamTake.js';
import { Route, BrowserRouter as Router, Link } from 'react-router-dom';
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned';
import TurnedInExam from './components/TurnedInExam.js';
import TurnedInExamDetail from './components/TurnedInExamDetail.js';
import { ContactSupport } from '@material-ui/icons';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

export default function App() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [addOpen, setAddOpen] = React.useState(false);
  const [name, setName] = React.useState(false);
  const [dueDate, setDueDate] = React.useState(false);
  const [exams, setExams] = React.useState([])
  const [turnedInExams, setTurnedInExams] = React.useState([])

  React.useEffect(() => {  
      fetchExamsData(); 
      fetchTurnedInExamsData();
  }, [])



  const fetchExamsData = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/exam',
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };

    
    axios(options)
      .then(response => {
        setExams(response.data.content)
      });
  };

  const fetchTurnedInExamsData = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/submittedexam',
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };

    
    axios(options)
      .then(response => {
        setTurnedInExams(response.data.content)
      });
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleAddOpen = () => {
    setAddOpen(true);
  };

  const handleAddClose = () => {
    setAddOpen(false);
  };
  
  const handleAddExam = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/exam',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
      data: {
        name: name,
        dueDate: (new Date(dueDate)).toISOString()
      }
    };

    console.log(options.data);
    
    axios(options)
      .then(response => {
        handleAddClose();
        fetchExamsData();
      });
  };

  return (
    <div className={classes.root}>
      <Dialog open={addOpen} onClose={handleAddClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Exam</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Exam Name"
            type="text"
            fullWidth
            onChange={event => setName(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="dueDate"
            label="Due Date"
            type="datetime-local"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            onChange={event => setDueDate(event.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAddClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleAddExam} color="primary">
            Add
          </Button>
        </DialogActions>
      </Dialog>

      

      

      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            OnlineExam
          </Typography>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
            <Route exact path="/">
              <IconButton onClick={handleAddOpen} color="inherit"><AddIcon /></IconButton>
            </Route>
            <Route path="/detail/:id">
              
            </Route>
              
            </div>
        </Toolbar>
      </AppBar>

      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
        <Link to="/" style={{ textDecoration: 'none' }}>
          <ListItem key="Exam">
            <ListItemIcon><AssignmentIcon /></ListItemIcon>
            <ListItemText primary="Exam"  />
          </ListItem>
        </Link>
        </List>
        <Divider />
        <List>
        <Link to="/exam/take" style={{ textDecoration: 'none' }}>
          <ListItem key="TakeExam">
            <ListItemIcon><AssignmentReturnedIcon /></ListItemIcon>
            <ListItemText primary="Take Exam"  />
          </ListItem>
        </Link>
        </List>
        <Divider />
        <List>
        <Link to="/submittedexam" style={{ textDecoration: 'none' }}>
          <ListItem button key="TurnedInExam">
              <ListItemIcon><AssignmentTurnedInIcon /></ListItemIcon>
              <ListItemText primary="Turned In Exam" />
            </ListItem>
          </Link>
        </List>
      </Drawer>

      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />

        
        <Route exact path="/">
          <ExamList exams={exams} fetchData={fetchExamsData}/>
        </Route>
        <Route exact path="/exam/take">
          <ExamList exams={exams} fetchData={fetchExamsData}/>
        </Route>
        <Route path="/detail/:id">
          <ExamDetail/>
        </Route>
        <Route path="/exam/take/:id">
          <ExamTake fetchData={fetchTurnedInExamsData}/>
        </Route>
        <Route exact path="/submittedexam">
          <TurnedInExam exams={turnedInExams} fetchData={fetchTurnedInExamsData}/>
        </Route>
        <Route path="/submittedexam/detail/:id">
          <TurnedInExamDetail />
        </Route>
        
        
        
      </main>
    </div>
  );
}