import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import {
  useParams,
} from "react-router-dom";
import TextField from '@material-ui/core/TextField';


export default function CreateQuestionDialog(props) {

  let { id } = useParams();

  const [question, setQuestion] = React.useState(false);
  const [answerA, setAnswerA] = React.useState(false);
  const [answerB, setAnswerB] = React.useState(false);
  const [answerC, setAnswerC] = React.useState(false);
  const [answerD, setAnswerD] = React.useState(false);
  const [answerTrue, setAnswerTrue] = React.useState(false);

  React.useEffect(() => {  
  }, [])
  

  const handleAdd = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/question',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
      data: {
        question: question,
        answerA: answerA,
        answerB: answerB,
        answerC: answerC,
        answerD: answerD,
        answerTrue: parseInt(answerTrue),
        examId: parseInt(id)
      }
    };

    console.log(options.data)
    
    axios(options)
      .then(response => {
        props.onClose()
      });
  }

  return (
    <Dialog open={props.open} onClose={props.onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Question</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="question"
            label="Question"
            type="text"
            fullWidth
            onChange={event => setQuestion(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="answerA"
            label="Answer A"
            type="text"
            fullWidth
            onChange={event => setAnswerA(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="answerB"
            label="Answer B"
            type="Text"
            fullWidth
            onChange={event => setAnswerB(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="answerC"
            label="Answer C"
            type="Text"
            fullWidth
            onChange={event => setAnswerC(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="answerD"
            label="Answer D"
            type="Text"
            fullWidth
            onChange={event => setAnswerD(event.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="answerTrue"
            label="Answer True"
            type="Text"
            fullWidth
            onChange={event => setAnswerTrue(event.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleAdd} color="primary">
            Add
          </Button>
        </DialogActions>
      </Dialog>

  );
  
}
  