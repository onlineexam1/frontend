import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
    useParams,
  } from "react-router-dom";
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CreateQuestionDialog from './CreateQuestionDialog.js';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
  examCard: {
    minWidth: 257
  },
}));


export default function ExamDetail(props) {
  const classes = useStyles();
  const theme = useTheme();

  let { id } = useParams();
  const [name, setName] = React.useState(false);
  const [dueDate, setDueDate] = React.useState(false)
  const [questions, setQuestions] = React.useState([])
  const [open, setOpen] = React.useState(false)
  const [confirmationOpen, setConfirmationOpen] = React.useState(false)
  const [question, setQuestion] = React.useState(false)

  React.useEffect(() => {  
    fetchData(); 
    fetchQuestionData();
  }, [])

  const handleClose = () => {
    setOpen(false)
    fetchData(); 
    fetchQuestionData();
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const handleConfirmationOpen = () => {
    setConfirmationOpen(true)
  }

  const handleConfirmationClose = () => {
    setConfirmationOpen(false)
  }

  const fetchData = () => {
    const options = {
        url: process.env.REACT_APP_BACKEND_HOST + '/exam/' + id,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8',
        },
      };
      
      axios(options)
        .then(response => {
          setName(response.data.content.name)
          setDueDate(response.data.content.dueDate)
        });
  }

  const fetchQuestionData = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/question/exam/' + id,
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };
    
    axios(options)
      .then(response => {
        setQuestions(response.data.content)
      });
  }

  const handleDelete = (questionId) => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST +'/question/' + questionId,
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };
    
    axios(options)
      .then(response => {
        fetchQuestionData();
        handleConfirmationClose();
      });
  }

  return (
    <div>
      <Dialog
        open={confirmationOpen}
        onClose={handleConfirmationClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Remove Question "{question.question}"</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to remove Question "{question.question}"?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleConfirmationClose} color="primary">
            No
          </Button>
          <Button onClick={(e) => {
            handleDelete(question.id)
          }}
          color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>

      <Card className={classes.examCard}>
          <CardActionArea>
          <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              {dueDate}
              </Typography>
          </CardContent>
          </CardActionArea>
      </Card>

      <br/>
      <CreateQuestionDialog open={open} onClose={handleClose}/>
      

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Question</TableCell>
              <TableCell align="right">Answer A</TableCell>
              <TableCell align="right">Answer B</TableCell>
              <TableCell align="right">Answer C</TableCell>
              <TableCell align="right">Answer D</TableCell>
              <TableCell align="right">True Answer</TableCell>
              <TableCell align="right">Action Button</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {questions.map((question) => (
              
              <TableRow key={question.id}>
                <TableCell component="th" scope="row">
                  {question.question}
                </TableCell>
                <TableCell align="right">{question.answerA}</TableCell>
                <TableCell align="right">{question.answerB}</TableCell>
                <TableCell align="right">{question.answerC}</TableCell>
                <TableCell align="right">{question.answerD}</TableCell>
                <TableCell align="right">{String.fromCharCode(65 + question.answerTrue % 4)}</TableCell>
                <TableCell align="right">

                  

                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    startIcon={<DeleteIcon />}
                    onClick={(e) => {
                      setQuestion(question)
                      handleConfirmationOpen()
                    }
                    }
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <br/>

      <Button
        variant="contained"
        color="primary"
        size="medium  "
        startIcon={<AddIcon />}
        onClick={handleOpen}
      >
        Add Question
      </Button>
    </div>
    
  );
  
}
  