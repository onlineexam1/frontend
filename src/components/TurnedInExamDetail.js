import React, { useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
    useParams,
  } from "react-router-dom";
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CreateQuestionDialog from './CreateQuestionDialog.js';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
  examCard: {
    minWidth: 257
  },
}));


export default function ExamDetail(props) {
  const classes = useStyles();
  const theme = useTheme();

  let { id } = useParams();
  const [name, setName] = React.useState(false);
  const [questions, setQuestions] = React.useState([])
  const [finalQuestions, setFinalQuestions] = React.useState([])
  const [open, setOpen] = React.useState(false)
  const [examName, setExamName] = React.useState(false)

  React.useEffect(() => {  
    fetchData(); 
    fetchQuestionData();
  }, [])

  const fetchData = () => {
    const options = {
        url: process.env.REACT_APP_BACKEND_HOST + '/submittedexam/' + id,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8',
        },
      };
      
      axios(options)
        .then(response => {
          setName(response.data.content.name)

          const options = {
            url: process.env.REACT_APP_BACKEND_HOST + '/exam/' + response.data.content.examId,
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json;charset=UTF-8',
            },
          };
          
          axios(options)
            .then(response => {
              setExamName(response.data.content.name)
            });
        });
  }

  const fetchQuestionData = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/submittedquestion/submittedexam/' + id,
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };
    
    axios(options)
      .then(response => {
        setQuestions(response.data.content)
      });
  }

  React.useEffect(() => {  
    updateWithQuestion()
  }, [questions])

  const updateWithQuestion = () => {

      var f_q = []

      questions.forEach((val) => {
        const options = axios({
            url: process.env.REACT_APP_BACKEND_HOST + '/question/' + val['questionId'],
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json;charset=UTF-8',
            },
          });

          f_q.push(options)

      })

      axios.all(f_q)
            .then(axios.spread((...responses) => {

                var q = questions

                responses.forEach((val) => {
                    for (var i = 0; i < q.length; i++) {

                        if (q[i].questionId != val.data.id) continue

                        q[i]['question'] = val.data.content.question
                        q[i]['answerA'] = val.data.content.answerA
                        q[i]['answerB'] = val.data.content.answerB
                        q[i]['answerC'] = val.data.content.answerC
                        q[i]['answerD'] = val.data.content.answerD
                        q[i]['answerTrue'] = val.data.content.answerTrue

                        break
                    }
                    
                })   
                
                setFinalQuestions(q)
            }));

      
  }


  return (
    <div>

      <Card className={classes.examCard}>
          <CardActionArea>
          <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              Exam {examName}
              </Typography>
          </CardContent>
          </CardActionArea>
      </Card>

      <br/>
      

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Question</TableCell>
              <TableCell align="right">Answer A</TableCell>
              <TableCell align="right">Answer B</TableCell>
              <TableCell align="right">Answer C</TableCell>
              <TableCell align="right">Answer D</TableCell>
              <TableCell align="right">True Answer</TableCell>
              <TableCell align="right">Answer</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {finalQuestions.map((question) => (
              
              <TableRow key={question.id}>
                <TableCell component="th" scope="row">
                  {question.question}
                </TableCell>
                <TableCell align="right">{question.answerA}</TableCell>
                <TableCell align="right">{question.answerB}</TableCell>
                <TableCell align="right">{question.answerC}</TableCell>
                <TableCell align="right">{question.answerD}</TableCell>
                <TableCell align="right">{String.fromCharCode(65 + question.answerTrue % 4)}</TableCell>
                <TableCell align="right">

                    {String.fromCharCode(65 + question.answer % 4)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
    
  );
  
}
  