import React from 'react';
import TurnedInExamCard from './TurnedInExamCard'
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  card: {
    margin: theme.spacing(2)
  }
}));

export default function TurnedInExam(props) {

  const classes = useStyles();

  const exams = props.exams

  return (
    <div className={classes.root}>
      <Grid container spacing={12}>
        {exams.map(exam =>
          <Grid xs={4} key={exam.id} m={"2rem"}>
              <TurnedInExamCard exam={exam} fetchData={props.fetchData} className={classes.card} />
          </Grid >
        )}
      </Grid>
    </div>
  );
}
  