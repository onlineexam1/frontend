import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import {
  useParams,
  Redirect
} from "react-router-dom";
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(3),
  },
  button: {
    margin: theme.spacing(1, 1, 0, 0),
  },
}));

export default function ExamTake(props) {
  let { id } = useParams()

  const classes = useStyles();
  const [value, setValue] = React.useState({name:'', examId: id, answers: {}});
  const [questions, setQuestions] = React.useState([]);
  const [name, setName] = React.useState(false);
  const [redirect, setRedirect] = React.useState(false);

  const handleRadioChange = (event) => {
    var new_value = value

    var val = event.target.value.split('-')

    new_value.answers[val[0]] = val[1]


    setValue(new_value);

    console.log(new_value)
  };

  React.useEffect(() => {  
    fetchData(); 
    fetchExamData();
  }, [])

  

  const fetchData = () => {
    const options = {
        url: process.env.REACT_APP_BACKEND_HOST + '/question/exam/' + id,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8',
        },
      };
      
      axios(options)
        .then(response => {
          setQuestions(response.data.content)
        });
  }

  const fetchExamData = () => {
    const options = {
        url: process.env.REACT_APP_BACKEND_HOST + '/exam/' + id,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8',
        },
      };
      
      axios(options)
        .then(response => {
          setName(response.data.content.name)
        });
  }

  const handleNameChange = (event) => {
    var new_value = value
    new_value['name'] = event.target.value

    setValue(new_value)
  }


  const handleSubmit = (event) => {
    event.preventDefault()
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/submittedexam',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
      data: {
        name: value.name,
        examId: parseInt(value.examId)
      }
    };
    
    axios(options)
      .then(response => {
        const submittedExamId = response.data.id

        var option_ = []

        console.log(Object.entries(value.answers))

        for (const [questionId, answer] of Object.entries(value.answers)) {
          var options2 = axios({
            url: process.env.REACT_APP_BACKEND_HOST + '/submittedanswer',
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json;charset=UTF-8',
            },
            data: {
              questionId: parseInt(questionId),
              submittedExamId: submittedExamId,
              answer: parseInt(answer)
            }
          });

          option_.push(options2);
        }

        axios.all(option_).then(axios.spread((...responses) => {
              props.fetchData()
              setRedirect(true)
            }
        ))
      });
  }; 

  if (redirect) {
    return <Redirect to="/submittedexam" />
  }

  return (

    
    <div>
    <Card className={classes.examCard}>
      <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          Exam {name}
          </Typography>
          <TextField label="Nama" onChange={handleNameChange}/>
      </CardContent>
    </Card>

    <br/>

    <form onSubmit={handleSubmit} className={classes.root}>
      <Grid container spacing={3}>
      {questions.map(question =>
      <Grid item={true} xs={12} key={question.id}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">{question.question}</FormLabel>
          <RadioGroup aria-label="quiz" name={question.id}  onChange={handleRadioChange}>
            <FormControlLabel value={question.id + "-0"} control={<Radio />} label={question.answerA} />
            <FormControlLabel value={question.id + "-1"} control={<Radio />} label={question.answerB} />
            <FormControlLabel value={question.id + "-2"} control={<Radio />} label={question.answerC} />
            <FormControlLabel value={question.id + "-3"} control={<Radio />} label={question.answerD} />
          </RadioGroup>
          
        </FormControl>
      </Grid>
      )}
      
      </Grid>
      <Button type="submit" variant="outlined" color="primary" className={classes.button}>
        Submit
      </Button>
      
    </form>
    </div>
  );
}