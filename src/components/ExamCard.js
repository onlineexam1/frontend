import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import {
  Link,
  Route
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({

}));


export default function ExamCard(props) {
  const classes = useStyles();
  const theme = useTheme();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const name = props.exam.name
  const dueDate = props.exam.dueDate
  const id = props.exam.id

  const handleDelete = () => {
    const options = {
      url: process.env.REACT_APP_BACKEND_HOST + '/exam/' + id,
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };
    
    axios(options)
      .then(response => {
        handleClose();
        props.fetchData();
      });
  }

  return (
    <div className={props.className}>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Remove Exam {name}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to remove exam {name}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            No
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
      <Card>
        
          <CardActionArea>
          <Route exact path="/">
          <Link to={"/detail/" + id} style={{ textDecoration: 'none' }}>
          <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              {dueDate}
              </Typography>
          </CardContent>
          </Link>
          </Route>
          <Route path="/exam/take">
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              {dueDate}
              </Typography>
            </CardContent>
          </Route>
          </CardActionArea>
         
          <CardActions>
          <Route exact path="/">
            <Button size="small" color="primary" onClick={handleClickOpen}>
                Delete
            </Button>
          </Route>

          <Route path="/exam/take">
            <Link to={"/exam/take/" + id} style={{textDecoration: 'none'}}>
            <Button size="small" color="primary">
                Take Exam
            </Button>
            </Link>
          </Route>
          
          </CardActions>
      </Card>
    </div>
    
  );
  
}
  