import React from 'react';
import ExamCard from './ExamCard'
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  card: {
    margin: theme.spacing(2)
  }
}));

export default function ExamList(props) {

  const classes = useStyles();

  const exams = props.exams

  return (
    <div className={classes.root}>
      <Grid container spacing={12}>
        {exams.map(exam =>
          <Grid xs={4} key={exam.id} m={"2rem"}>
              <ExamCard exam={exam} fetchData={props.fetchData} className={classes.card} />
          </Grid >
        )}
      </Grid>
    </div>
  );
}
  